# -*- coding: utf-8 -*-
"""
Created on Sun Nov  5 00:14:33 2017

@author: slava
"""
from datetime import datetime
def size_str(size):
    
    size_info = ''
    if size < 1024:
        size_info = str(size) + ' ' + 'Байт'
    elif (size > 1024) and (size < 1024*1024):
        size_info =  str(size // 1024) + ' '+ 'КБайт'
    elif (size > 1024*1024) and (size < 1024*1024*1024):
        size_info = str(size // (1024*1024))+' '+ 'МБайт'
    else:
        size_info = str(size // (1024*1024*1024))+' '+ 'ГБайт' 
                       
    return size_info

def rev_date(date):
    
    str_date = ''
    for i in date:
        
        if i == '/':          
            i = '.'
        str_date += i
    
    ret_date = datetime.strptime(str_date, '%d.%m.%Y')
    return ret_date