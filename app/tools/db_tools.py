# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 12:24:07 2017

@author: slava
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import os
import sys
sys.path.append(os.path.realpath('tools'))

from tools.class_db import Files, Logsmessage, Message, db_params#, Filedir
from datetime import datetime
from tools.work_functions import size_str, rev_date

import psycopg2

def create_session(sql):
    
    engine = create_engine(sql,  connect_args={'connect_timeout': 100})

    Session = sessionmaker(bind = engine)
    session = Session()  

    return (session)

def create_connection():
    
    connection = psycopg2.connect(dbname = db_params[3], user = db_params[1],
                                  password = db_params[2], host = db_params[0],
                                  port = db_params[4])
    connection.set_isolation_level(psycopg2.extensions.\
                                   ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = connection.cursor()
    return cursor, connection


def get_info(session, date_filter, row_filter):
    
    if (date_filter == '') and (row_filter == 0): 
    
        files = session.query(Files).\
                        order_by(Files.id_files.desc()).\
                        filter(Files.id_files == Logsmessage.id_files).\
                        limit(100)

        info_files = []
        for file in files:
#            print(file)
            
            
            log_message = session.query(Logsmessage).\
                          order_by(Logsmessage.id_message.desc()).\
                          filter(Logsmessage.id_files == file.id_files).first()
            message = session.query(Message).\
                    filter(Message.id_message == log_message.id_message).first()
            mess = message.message        
            
            date = datetime.strftime(log_message.datetime, "%d.%m.%y %H:%M:%S")
            
            size = size_str(file.filesize)
            
                
            buffer = {'date':date, 'name':file.filename,'size':size,
                      'source':file.filename[0:2], 
                      'abonent':file.filename[2:4], 'message':mess}
                
            info_files.append(buffer)

        return info_files         
#////////////////////////////                                              
    elif (date_filter != '') or (row_filter != 0):
     
        if (date_filter == ''):
            
            files = session.query(Files).\
                        order_by(Files.id_files.desc()).\
                        filter(Files.id_files == Logsmessage.id_files).\
                        limit(row_filter)
            info_files = []
            for file in files:

            
                log_message = session.query(Logsmessage).\
                          order_by(Logsmessage.id_message.desc()).\
                          filter(Logsmessage.id_files == file.id_files).first()
                message = session.query(Message).\
                    filter(Message.id_message == log_message.id_message).first()
                mess = message.message        
            
                date = datetime.strftime(log_message.datetime, "%d.%m.%y %H:%M:%S")
            
                size = size_str(file.filesize)
            
            
                buffer = {'date':date, 'name':file.filename,'size':size,
                      'source':file.filename[0:2], 
                      'abonent':file.filename[2:4], 'message':mess}
                
                info_files.append(buffer)

            return info_files       
    
        elif (row_filter == 0):
            
            files = session.query(Files).\
                        order_by(Files.id_files.desc()).\
                        filter(Files.id_files == Logsmessage.id_files).\
                        filter(Logsmessage.datetime >= rev_date(date_filter)).\
                        limit(100)
            info_files = []
            for file in files:

            
                log_message = session.query(Logsmessage).\
                          order_by(Logsmessage.id_message.desc()).\
                          filter(Logsmessage.id_files == file.id_files).first()
                message = session.query(Message).\
                    filter(Message.id_message == log_message.id_message).first()
                mess = message.message        
            
                date = datetime.strftime(log_message.datetime, "%d.%m.%y %H:%M:%S")
            
                size = size_str(file.filesize)
            
            
                buffer = {'date':date, 'name':file.filename,'size':size,
                      'source':file.filename[0:2], 
                      'abonent':file.filename[2:4], 'message':mess}
                
                info_files.append(buffer)

            return info_files       
    
    elif (date_filter != '') and (row_filter != 0):

        files = session.query(Files).\
                        order_by(Files.id_files.desc()).\
                        filter(Files.id_files == Logsmessage.id_files).\
                        filter(Logsmessage.datetime >= rev_date(date_filter)).\
                        limit(row_filter)
        
        info_files = []
        for file in files:

            
            log_message = session.query(Logsmessage).\
                          order_by(Logsmessage.id_message.desc()).\
                          filter(Logsmessage.id_files == file.id_files).first()
            message = session.query(Message).\
                    filter(Message.id_message == log_message.id_message).first()
            mess = message.message        
            
            date = datetime.strftime(log_message.datetime, "%d.%m.%y %H:%M:%S")
            
            size = size_str(file.filesize)
            
            
            buffer = {'date':date, 'name':file.filename,'size':size,
                      'source':file.filename[0:2], 
                      'abonent':file.filename[2:4], 'message':mess}
                
            info_files.append(buffer)

        return info_files       

def get_info_2(cursor, date_b, date_f, rows):
    
   
    select = '''
            SELECT b.datetime, b.station_log, b.station_abn, a.filesize,
            a.filename, b.id_message, e.message 
            FROM files a
            LEFT JOIN logsmessage b 
            ON a.id_files=b.id_files 
            AND datetime > \'{}\'
            AND datetime < \'{}\'
            AND b.id_files NOTNULL 
            AND b.id_message = 
                (SELECT id_message 
                FROM logsmessage c 
                WHERE b.id_files=c.id_files 
                ORDER BY datetime DESC
                LIMIT 1)
        LEFT JOIN message e
        ON e.id_message = b.id_message
        WHERE b.id_message NOTNULL ORDER BY a.id_files DESC
        LIMIT {}
        '''.format(date_b, date_f, rows)
     
    cursor.execute(select)

    rows = cursor.fetchall()

    info_ = []
    for row in rows:
        
        buffer = {'date': datetime.strftime(row[0], "%d.%m.%y %H:%M:%S"),
                  'source': ' ' + row[4][0:2], 'abonent': ' ' +row[4][2:4],
                  'size': ' ' + size_str(row[3]), 'name': ' ' +row[4], 
                  'message': ' ' + str(row[6])}
        info_.append(buffer)
    
    
    
    
    return info_
        
    
    
    
    