# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 14:04:45 2017

@author: slava
"""
import os.path
import sys
import configparser


sys.path.append(os.path.realpath('app'))
Config = configparser.ConfigParser()
Config.read(os.path.realpath('settings.ini'))

from app import app
from flask_sqlalchemy import SQLAlchemy
#from flask_login import AnonymousUserMixin

from sqlalchemy.ext.declarative import declarative_base  
from sqlalchemy import Column, String, Integer,DateTime, SmallInteger, BigInteger


db_params = [Config['DB_PARAMS']['host'], Config['DB_PARAMS']['user'], 
             Config['DB_PARAMS']['password'], Config['DB_PARAMS']['database'],
                   Config['DB_PARAMS']['port']] 

db = SQLAlchemy(app)

base = declarative_base()

class User(db.Model):
    
    __tablename__ = Config['TAB_NAMES']['tn_users']
    id_user = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    fullname = db.Column(db.String)
    email = db.Column(db.String)
    nick_name = db.Column(db.String)
    password = db.Column(db.String)
    is_active = db.Column(db.Boolean, default = False)
    
    def __init__(self, name, fullname, email, nick_name, password, is_active):
        
        self.name = name
        self.fullname = fullname
        self.email = email
        self.nick_name = nick_name
        self.password = password
        self.is_active = is_active
        
    def __repr__(self):
        return "User_%s (%s| %s| %s| %s)" %(str(self.id_user), self.name,
                         self.fullname, self.email, self.nick_name )
    
    def get_id(self):
        return str(self.id_user)
    
    def is_active(self):
        return self.is_active
    
    def is_authenticated(self):
        return True

    def revers_to(self):
        
        info_user = {'name': self.name, 'fullname': self.fullname,
                     'email': self.email, 'nick':self.nick_name, 'fn':self.fullname[0:1]+self.name[0:1]}
        return info_user
        


class Filedir(base):
    
    __tablename__ = 'filedir'
    id_filedir = Column(Integer, primary_key = True)
    dirname = Column(String)
    
class Files(base):
    
    __tablename__ = 'files'
    id_files = Column(Integer, primary_key = True)
    filename = Column(String)
    pr_d = Column(SmallInteger)
    id_dir = Column(Integer)
    filesize = Column(BigInteger)
    datemodif = Column(DateTime)
    hash = Column(BigInteger)
    
    def __repr__(self):
        return " File %s %s" %(str(self.id_files), self.filename)

class Logsmessage(base):
    
    __tablename__ = 'logsmessage'
    id_logsmessage = Column(Integer, primary_key = True)
    datetime = Column(DateTime)
    station_log = Column(Integer)
    id_message = Column(Integer)
    station_abn = Column(Integer)
    dirobm = Column(Integer)
    doppole1 = Column(String)
    doppole2 = Column(String)
    doppole3 = Column(String)
    id_files = Column(Integer)
    timecomplete = Column(BigInteger)
    
    def __repr__(self):
        
        return "Message_%s (%s| %s| %s)" %( str(self.id_logsmessage),
                            str(self.id_message), 
                        str(self.id_files), str(self.datetime) )
    
class Message(base):
    
    __tablename__ = 'message'
    id_message = Column(Integer, primary_key = True)
    id_typemessage = Column(Integer)
    message = Column(String)
    
class Station(base):
    
    __tablename__ = 'station'
    id_station = Column(Integer, primary_key = True)
    name_station = Column(String)
    
class Stationip(base):
    
    __tablename__ = 'stationip'
    id_stationip = Column(Integer, primary_key = True)
    id_station = Column(Integer)
    ipaddres = Column(String)
    port = Column(Integer)
    
class Typemessage(base):
    
    __tablename__ = 'typemessage'
    id_typemessage = Column(Integer, primary_key = True)
    type = Column(String(50))
    
    