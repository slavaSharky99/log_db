# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 15:39:35 2017

@author: slava
"""

#from gevent import monkey
#monkey.patch_all()

from flask import Flask
from flask_socketio import SocketIO


app = Flask(__name__, template_folder = 'template', static_folder = 'static')
socketio = SocketIO(app, manage_session=True)
#thread = None

app.secret_key = 'SECRET'

from app import server