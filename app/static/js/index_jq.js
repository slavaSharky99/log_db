function sort_date()
{
  var tr = $('#body_table');

  var table = document.getElementsByTagName("tbody");
  var rows = table[0].rows;
  var mass_tr = []
  var help_mass = []

  var class_span_date = $('#span_date').attr('class');

  for (var i = 0; i < rows.length; i++)
  {
      var str_ = rows[i].textContent.split(' ');
      var major_str = str_[0] + str_[1];
      help_mass.push({ str_: major_str, value: i});
      mass_tr.push(rows[i].innerHTML);
  }
      /*alphabet A-Z*/
  if ( class_span_date == 'glyphicon glyphicon-chevron-up')
  {
    $('#span_date').removeClass('glyphicon glyphicon-chevron-up');
    $('#span_date').addClass('glyphicon glyphicon-chevron-down');
    help_mass.sort(function (a,b){

        if (a.str_ > b.str_){
          return 1;
        }
        if (a.str_ < b.str_) {
          return -1;
        }
        return 0;
    });
  } else{
    $('#span_date').removeClass('glyphicon glyphicon-chevron-down');
    $('#span_date').addClass('glyphicon glyphicon-chevron-up');
    help_mass.sort(function (a,b){

        if (a.str_ < b.str_){
          return 1;
        }
        if (a.str_ > b.str_) {
          return -1;
        }
        return 0;
    });
  }

  tr.html("");
  for ( var i = 0; i < help_mass.length; i++)
  {
      for ( var j = 0; j < mass_tr.length; j++)
      {
          if ( help_mass[i].value == j)
          {
              tr.append('<tr  id = "id_info">' + mass_tr[j]+ '</tr>');
          }
      }
  }

}

function sort_source()
{
  var tr = $('#body_table');
  var table = document.getElementsByTagName("tbody");
  var rows = table[0].rows;
  var mass_tr = []
  var help_mass = []

  var class_span_source = $('#span_source').attr('class');

  for (var i = 0; i < rows.length; i++)
  {
      var str_ = rows[i].textContent.split(' ');
      var major_str = str_[2];
      help_mass.push({ str_: major_str, value: i});
      mass_tr.push(rows[i].innerHTML);
  }
      /*alphabet A-Z*/
  if ( class_span_source == 'glyphicon glyphicon-chevron-up')
  {
    $('#span_source').removeClass('glyphicon glyphicon-chevron-up');
    $('#span_source').addClass('glyphicon glyphicon-chevron-down');
    help_mass.sort(function (a,b){

        if (a.str_ > b.str_){
          return 1;
        }
        if (a.str_ < b.str_) {
          return -1;
        }
        return 0;
    });
  } else
  {
    $('#span_source').removeClass('glyphicon glyphicon-chevron-down');
    $('#span_source').addClass('glyphicon glyphicon-chevron-up');
    help_mass.sort(function (a,b){

        if (a.str_ < b.str_){
          return 1;
        }
        if (a.str_ > b.str_) {
          return -1;
        }
        return 0;
    });
  }

  tr.html("");
  for ( var i = 0; i < help_mass.length; i++)
  {
      for ( var j = 0; j < mass_tr.length; j++)
      {
          if ( help_mass[i].value == j)
          {
              tr.append('<tr  id = "id_info">' + mass_tr[j]+ '</tr>');
          }
      }
  }

}
function sort_abonent()
{
  var tr = $('#body_table');
  var table = document.getElementsByTagName("tbody");
  var rows = table[0].rows;
  var mass_tr = []
  var help_mass = []

  var class_span_abonent = $('#span_abonent').attr('class');

  for (var i = 0; i < rows.length; i++)
  {
      var str_ = rows[i].textContent.split(' ');
      var major_str = str_[3];
      help_mass.push({ str_: major_str, value: i});
      mass_tr.push(rows[i].innerHTML);
  }
      /*alphabet A-Z*/
  if ( class_span_abonent == 'glyphicon glyphicon-chevron-up')
  {
    $('#span_abonent').removeClass('glyphicon glyphicon-chevron-up');
    $('#span_abonent').addClass('glyphicon glyphicon-chevron-down');
    help_mass.sort(function (a,b){

        if (a.str_ > b.str_){
          return 1;
        }
        if (a.str_ < b.str_) {
          return -1;
        }
        return 0;
    });
  } else
  {
    $('#span_abonent').removeClass('glyphicon glyphicon-chevron-down');
    $('#span_abonent').addClass('glyphicon glyphicon-chevron-up');
    help_mass.sort(function (a,b){

        if (a.str_ < b.str_){
          return 1;
        }
        if (a.str_ > b.str_) {
          return -1;
        }
        return 0;
    });
  }

  tr.html("");
  for ( var i = 0; i < help_mass.length; i++)
  {
      for ( var j = 0; j < mass_tr.length; j++)
      {
          if ( help_mass[i].value == j)
          {
              tr.append('<tr  id = "id_info">' + mass_tr[j]+ '</tr>');
          }
      }
  }
}

function sort_size ()
{
  var tr = $('#body_table');
  var table = document.getElementsByTagName("tbody");
  var rows = table[0].rows;
  var mass_tr = []
  var help_mass = []

  var class_span_size = $('#span_size').attr('class');

  for (var i = 0; i < rows.length; i++)
  {
      var str_ = rows[i].textContent.split(' ');
      if ( str_[5] == 'Байт'){
        var major_str = parseInt(str_[4]);
      }
      if ( str_[5] == 'КБайт'){
        var major_str = parseInt(str_[4])*1024;
      }
      if ( str_[5] == 'МБайт'){
        var major_str = parseInt(str_[4])*1024*1024;
      }
      if ( str_[5] == 'ГБайт'){
        var major_str = parseInt(str_[4])*1024*1024*1024;
      }
      help_mass.push({ str_: major_str, value: i});
      mass_tr.push(rows[i].innerHTML);
  }
  if ( class_span_size == 'glyphicon glyphicon-chevron-up')
  {
    $('#span_size').removeClass('glyphicon glyphicon-chevron-up');
    $('#span_size').addClass('glyphicon glyphicon-chevron-down');
    help_mass.sort(function (a,b){

        if (a.str_ > b.str_){
          return 1;
        }
        if (a.str_ < b.str_) {
          return -1;
        }
        return 0;
    });
  } else
  {
    $('#span_size').removeClass('glyphicon glyphicon-chevron-down');
    $('#span_size').addClass('glyphicon glyphicon-chevron-up');
    help_mass.sort(function (a,b){

        if (a.str_ < b.str_){
          return 1;
        }
        if (a.str_ > b.str_) {
          return -1;
        }
        return 0;
    });
  }

  tr.html("");
  for ( var i = 0; i < help_mass.length; i++)
  {
      for ( var j = 0; j < mass_tr.length; j++)
      {
          if ( help_mass[i].value == j)
          {
              tr.append('<tr  id = "id_info">' + mass_tr[j]+ '</tr>');
          }
      }
  }
}
function sort_name()
{
  var tr = $('#body_table');
  var table = document.getElementsByTagName("tbody");
  var rows = table[0].rows;
  var mass_tr = []
  var help_mass = []

  var class_span_name = $('#span_name').attr('class');

  for (var i = 0; i < rows.length; i++)
  {
      var str_ = rows[i].textContent.split(' ');
      var major_str = str_[6];
      help_mass.push({ str_: major_str, value: i});
      mass_tr.push(rows[i].innerHTML);
  }
      /*alphabet A-Z*/
if ( class_span_name == 'glyphicon glyphicon-chevron-up')
{
    $('#span_name').removeClass('glyphicon glyphicon-chevron-up');
    $('#span_name').addClass('glyphicon glyphicon-chevron-down');
    help_mass.sort(function (a,b){

        if (a.str_ > b.str_){
          return 1;
        }
        if (a.str_ < b.str_) {
          return -1;
        }
        return 0;
    });
  } else
  {
    $('#span_name').removeClass('glyphicon glyphicon-chevron-down');
    $('#span_name').addClass('glyphicon glyphicon-chevron-up');
    help_mass.sort(function (a,b){

        if (a.str_ < b.str_){
          return 1;
        }
        if (a.str_ > b.str_) {
          return -1;
        }
        return 0;
    });
  }

  tr.html("");
  for ( var i = 0; i < help_mass.length; i++)
  {
      for ( var j = 0; j < mass_tr.length; j++)
      {
          if ( help_mass[i].value == j)
          {
              tr.append('<tr  id = "id_info">' + mass_tr[j]+ '</tr>');
          }
      }
  }
}
function sort_column()
{
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("tab");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
function change_span_status()
{
  var class_span_status = $('#span_status').attr('class');

  if ( class_span_status == 'glyphicon glyphicon-chevron-up')
  {
      $('#span_status').removeClass('glyphicon glyphicon-chevron-up');
      $('#span_status').addClass('glyphicon glyphicon-chevron-down');
  }else{
    $('#span_status').removeClass('glyphicon glyphicon-chevron-down');
    $('#span_status').addClass('glyphicon glyphicon-chevron-up');
  }
}
