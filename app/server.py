# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 15:34:19 2017

@author: slava
"""

import sys
import os
sys.path.append(os.path.realpath('app'))

from flask import render_template,  request



from app import app, socketio#, thread
from flask_socketio import disconnect
import functools
import json
from datetime import datetime, timedelta

from app.tools import class_db, db_tools
import time
sql_postgres = 'postgresql://' + class_db.db_params[1] + ':' +\
                    class_db.db_params[2] + '@' + class_db.db_params[0] +\
                        '/' + class_db.db_params[3]


app.config['SQLALCHEMY_DATABASE_URI'] = sql_postgres
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

users = []
i = 0
session = db_tools.create_session(sql_postgres)
cur, conn = db_tools.create_connection()


@app.route('/')
def login_show():
    
    return render_template('login.html')

@socketio.on('addUser')
def login(user):

    if user['type'] == 'log_in':
        
        user_in = session.query(class_db.User).filter_by(
                                nick_name = user['name'], 
                               password = user['password']
                               ).first()
#        print(request.sid)

        if (user_in):
            
            users.append({'user': user_in, 'id': user['index'], 'un_id': None})
            
            data = {'status':'Ok', 'id':user['index']}

            socketio.emit('next', data)
            
            
    if user['type'] == 'reg':

        if not session.query(class_db.User).filter_by(
                            name = user['name'],
                            fullname = user['fullname'], 
                            email = user['email'],
                            nick_name = user['nick'],
                            password = user['password']
                            ).first():
            
            new_user = class_db.User(user['name'], user['fullname'],
                                     user['email'], user['nick'],
                                         user['password'], True)
            session.add(new_user)
            session.commit()
            users.append({'user':new_user, 'id': user['index'], 'un_id': None})

            socketio.emit('reg', user, broadcast = True)
  
def authenticated_only(f):
    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        for user in users:
            if not user['id']:
                disconnect()
            else:
                
                return f(*args, **kwargs)
    return wrapped



@app.route('/index<id>')
@authenticated_only
def log_show(id):
    
    global index
    index = id
#    log = db_tools.get_info(session, '', 0)
    
    date = datetime.strftime(datetime.now(), "%d.%m.%Y")
    yesterday = datetime.now() - timedelta(days =1)
    log = db_tools.get_info_2(cur, yesterday.strftime("%d.%m.%Y") , date, 100 )

    return render_template('index.html', result = log, date = date, yesterday = yesterday.strftime("%d.%m.%Y"))   
#    return render_template('devexp/index1.html', data = log)
     
#@app.route('/index<id>', methods = ["GET", "POST"])
#@authenticated_only
#def interrogator(id):
#    
#    date_filter = request.form['datetime']
#    if request.form['rows'] != '': 
#        rows = int(request.form['rows'])
#    else:
#        rows = 0
#    print(date_filter, rows)
#    log = db_tools.get_info(session, date_filter, rows)
#    print(log)
#    return json.dumps(log)

@socketio.on('connect', namespace='/index')
@authenticated_only
def test():
    
    for user in users:
        
        try:
            
            if not user['un_id']:
                
                user['un_id'] = request.sid
                
            else:
                
                if user['un_id'] != request.sid:
                    
                    user.update(un_id=request.sid)
        
        except KeyError as e:
            
            user.update(['un_id',request.sid])
    
    
    socketio.emit('get_user', request.sid, namespace='/index', room = request.sid )

@socketio.on('log', namespace='/index')
@authenticated_only
def index():
    i = 0
    
    for user in users:
        
        if request.sid == user['un_id']:
            
            if str(user['id']) == index:
                ret_user = users[i]['user'].revers_to()

            i += 1
    print(ret_user)    
    socketio.emit('data', 
                  {'id':request.sid, 'user':ret_user }, 
                   namespace='/index', room = request.sid)
    
    
@socketio.on('request', namespace='/index')
@authenticated_only
def req_ans(data):
    
    date0 = data['date0']
    date1 = data['date1']
    rows = data['rows']
    
    if date0 != '' and date1 != '' and rows != '':
        rows = int(rows)
    elif rows == '':
        rows = 100
        
#    log = db_tools.get_info(session, date_filter, rows)

    log = db_tools.get_info_2(cur, date0, date1, rows)
    print(log, "LOG")
    socketio.emit('answer', log, namespace='/index', room = request.sid)
   

#@socketio.on('update', namespace='/index')
#@authenticated_only
#def updater():
#    
#    
#    buffer = "D:/Projects/Python/Work/Project_2/buffer.txt"
#       
#    while True:
#        
#        a = os.path.getmtime(buffer)
#        time.sleep(1)
#        b = os.path.getmtime(buffer)
#
#        
#        if b > a:
#            
#            log = db_tools.get_info(session,'',0)
#            socketio.emit('answer', log, namespace='/index',
#                          room = request.sid)
    
    