# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 15:41:46 2017

@author: slava
"""
import sys
import os
sys.path.append(os.path.realpath('app'))
from app import app, socketio

if __name__ == '__main__':
    
    socketio.run(app, host = '0.0.0.0', port = 5000)
    